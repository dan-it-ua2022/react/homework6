import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Layout } from 'src/Layout/Layout';
import { getData } from 'src/redux/extraReducers/getData';
import {
  setFavorites,
  setProductsInBasket,
} from 'src/redux/reducers/productsThunk';
import { ToggleProductsViewProvider } from 'src/contexts/ToggleProductsViewProvider';

// load favorites from localStorage
const favoriteIdsFromStorage = localStorage.getItem('favorites');
// load products in basket from localStorage
const basketIdsFromStorage = localStorage.getItem('basket');

// initial App
export const App = () => {
  const dispatch = useDispatch();

  /********************** LOAD PRODUCTS **********************/
  // get data
  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  /********************** MOUNTING COMPONENT **********************/
  // load when App is mounting
  useEffect(() => {
    // load favorites
    if (favoriteIdsFromStorage) {
      dispatch(setFavorites(JSON.parse(favoriteIdsFromStorage)));
    }

    // load basket
    if (basketIdsFromStorage) {
      dispatch(setProductsInBasket(JSON.parse(basketIdsFromStorage)));
    }
  }, [dispatch]);

  /********************** RENDER **********************/
  return (
    <ToggleProductsViewProvider>
      <Layout />
    </ToggleProductsViewProvider>
  );
};
