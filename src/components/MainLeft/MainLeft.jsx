import React from 'react';
import styles from './MainLeft.module.scss';

export const MainLeft = () => {
  return (
    <div className={styles.MainLeft}>
      <h1>Filter</h1>
      <ul className={styles.filter_body}>
        <li className={styles.filter_item}>Color</li>
        <li className={styles.filter_item}>Size</li>
        <li className={styles.filter_item}>Weight</li>
        <li className={styles.filter_item}>Connect</li>
      </ul>
    </div>
  );
};
